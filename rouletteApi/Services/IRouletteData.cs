﻿using rouletteApi.Models;
using System.Collections.Generic;

namespace rouletteApi.Services
{
    public interface IRouletteData
    {
        public Roulette SaveRoulette(Roulette roulette);        
        public Roulette FindRoulette(string id);
        public void OpenRoulette(Roulette roulette);
        public void BetRoulette(Roulette roulette);
        public void CloseRoulette(Roulette roulette);
        public List<RouletteList> GetAllRoulette();
    }
}
