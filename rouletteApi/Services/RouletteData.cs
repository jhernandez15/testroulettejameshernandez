﻿using EasyCaching.Core;
using rouletteApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rouletteApi.Services
{
    public class RouletteData : IRouletteData
    {
        private IEasyCachingProvider cachingProvider;

        private const string Prefix = "Roulette";

        public RouletteData(IEasyCachingProvider cachingProvider)
        {
            this.cachingProvider = cachingProvider;
        }

        public Roulette SaveRoulette(Roulette roulette)
        {
            cachingProvider.Set(Prefix + roulette.Id, roulette, TimeSpan.FromDays(1));
            return roulette;
        }

        public Roulette FindRoulette(string id)
        {
            Roulette roulette = new Roulette();
            var item = this.cachingProvider.Get<Roulette>(Prefix + id);
            
            if (!item.HasValue)
            {
                return null;
            }
            else
            {
                roulette = item.Value;
            }

            return roulette;
        }

        public void OpenRoulette(Roulette roulette)
        {
            SaveRoulette(roulette);
        }

        public void BetRoulette(Roulette roulette)
        {
            SaveRoulette(roulette);
        }

        public void CloseRoulette(Roulette roulette)
        {
            SaveRoulette(roulette);
        }

        public List<RouletteList> GetAllRoulette()
        {
            var rouletes = this.cachingProvider.GetByPrefix<Roulette>(Prefix);
            List<Roulette> listRoulette = new List<Roulette>();
            List<RouletteList> listRouletteList = new List<RouletteList>();
            RouletteList rouletteList;

            if (rouletes.Values.Count > 0)
            {
                listRoulette = rouletes.Select(x => x.Value.Value).ToList();
                foreach (Roulette item in listRoulette)
                {
                    rouletteList = new RouletteList();
                    rouletteList.Id = item.Id;
                    rouletteList.IsOpen = item.IsOpen;
                    listRouletteList.Add(rouletteList);
                }
            }

            return listRouletteList;
        }
    }
}
