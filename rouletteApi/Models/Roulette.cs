﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace rouletteApi.Models
{
    [Serializable]
    public class Roulette
    {
        public string Id { get; set; }
        public bool IsOpen { get; set; }
        public DateTime Opening { get; set; }
        public DateTime Closing { get; set; }

        public List<Bet> Bets = new List<Bet>();
    }
}
