﻿using rouletteApi.Models;
using System.Collections.Generic;

namespace rouletteApi.Services
{
    public interface IRouletteService
    {
        public Roulette CreateRoulette();

        public Result OpenRoulette(string Id);

        public Roulette CloseRoulette(string Id);

        public Result BetRoulette(string id, string userId, Bet userBet);

        public List<RouletteList> GetAllRoulette();
    }
}
