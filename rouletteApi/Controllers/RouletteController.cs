﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using rouletteApi.Models;
using rouletteApi.Services;

namespace rouletteApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private IRouletteService rouletteService;

        public RouletteController(IRouletteService rouletteService)
        {
            this.rouletteService = rouletteService;
        }

        [HttpPost]
        public IActionResult NewRoulette()
        {
            Roulette roulette = rouletteService.CreateRoulette();
            return Ok(roulette);
        }

        [HttpPut("{id}/open")]
        public IActionResult OpenRoulette([FromRoute(Name = "id")] string id)
        {
            Result result = rouletteService.OpenRoulette(id);
            return Ok(result);
        }

        [HttpPost("{id}/bet")]
        public IActionResult Bet([FromHeader(Name = "userId")] string userId, [FromRoute(Name = "id")] string id, Bet request)
        {
            Result result = rouletteService.BetRoulette(id, userId, request);
            return Ok(result);
        }

        [HttpPut("{id}/close")]
        public IActionResult CloseRoulette([FromRoute(Name = "id")] string id)
        {
            Roulette roulette = rouletteService.CloseRoulette(id);
            return Ok(roulette);
        }

        [HttpGet]
        public IActionResult GetAllRoulette()
        {
            return Ok(rouletteService.GetAllRoulette());
        }
    }
}
