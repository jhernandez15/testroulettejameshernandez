﻿using rouletteApi.Models;
using System;
using System.Collections.Generic;

namespace rouletteApi.Services
{
    public class RouletteService : IRouletteService
    {
        private IRouletteData rouletteData;

        public RouletteService(IRouletteData rouletteData)
        {
            this.rouletteData = rouletteData;
        }

        public Roulette CreateRoulette()
        {
            Roulette roulette = new Roulette();
            roulette.Id = Guid.NewGuid().ToString();
            roulette.IsOpen = false;
            rouletteData.SaveRoulette(roulette);
            return roulette;
        }

        public Result OpenRoulette(string id)
        {
            Result result = new Result();
            Roulette roulette = rouletteData.FindRoulette(id);

            if (roulette == null)
            {
                result.Message = "Operation rejected";
            }
            else if (roulette.IsOpen)
            {
                result.Message = "Operation rejected";
            }
            else if(roulette.Opening != null)
            {
                result.Message = "Operation rejected";
            }
            else
            {
                roulette.IsOpen = true;
                roulette.Opening = DateTime.Now;
                rouletteData.OpenRoulette(roulette);
                result.Message = "Successful operation";
            }

            return result;
        }

        public Result BetRoulette(string id, string userId, Bet userBet)
        {
            Result result = new Result();
            Roulette roulette = rouletteData.FindRoulette(id);

            if (roulette == null)
            {
                result.Message = "Operation rejected";
            }
            else if (!roulette.IsOpen)
            {
                result.Message = "Roulette closed";
            }
            else if(userBet.Money < 1 || userBet.Money > 10000)
            {
                result.Message = "Bet value not allowed";
            }
            else if(userBet.Number < 0 || userBet.Number > 36)
            {
                result.Message = "Number not allowed";
            }
            else if (userBet.Color.ToUpper() != "BLACK" && userBet.Color.ToUpper() != "RED")
            {
                result.Message = "Color not allowed";
            }
            else
            {
                userBet.User = Convert.ToInt32(userId);
                roulette.Bets.Add(userBet);
                rouletteData.BetRoulette(roulette);
                result.Message = "Bet accepted";
            }

            return result;
        }

        public Roulette CloseRoulette(string id)
        {
            Roulette roulette = rouletteData.FindRoulette(id);

            if (roulette == null)
            {
                throw new Exception("Operation rejected");
            }
            else if (!roulette.IsOpen)
            {
                throw new Exception("Operation rejected");
            }
            else
            {
                roulette.IsOpen = false;
                roulette.Closing = DateTime.Now;
                rouletteData.CloseRoulette(roulette);
                Random random = new Random();
                int winningNumber = random.Next(0, 36);
                string winningColor = string.Empty;
                if (winningNumber == 0)
                {
                    winningColor = "RED";
                }
                else
                {
                    winningColor = (winningNumber % 2 == 0) ? "RED" : "BLACK";
                }
                foreach (Bet item in roulette.Bets)
                {
                    item.Win = 0;
                    if (item.Number == winningNumber)
                    {
                        item.Win = item.Money * 5;
                    }
                    if (item.Color == winningColor)
                    {
                        item.Win+= item.Money * 1.8;
                    }
                }
            }

            return roulette;
        }

        public List<RouletteList> GetAllRoulette()
        {
            return rouletteData.GetAllRoulette();
        }
    }
}
