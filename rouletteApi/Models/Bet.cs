﻿using System;

namespace rouletteApi.Models
{
    [Serializable]
    public class Bet
    {
        public int User { get; set; }
        public int Number { get; set; }
        public string Color { get; set; }
        public double Money { get; set; }
        public double Win { get; set; }
    }
}
