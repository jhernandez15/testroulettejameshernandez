﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rouletteApi.Models
{
    public class RouletteList
    {
        public string Id { get; set; }
        public bool IsOpen { get; set; }
    }
}
